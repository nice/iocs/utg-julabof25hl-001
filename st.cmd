#!/usr/bin/env iocsh.bash

# This should be a test startup script
require julabof25hl

# Set parameters
epicsEnvSet("IPADDR",       "172.30.244.140")
epicsEnvSet("IPPORT",       "4001")
epicsEnvSet("SYS",          "Utg-SEPool")
epicsEnvSet("DEV",          "SEE-FTCtrl-JU02")
epicsEnvSet("PORTNAME",     "$(SYS)-$(DEV)")
epicsEnvSet("P",            "$(SYS):")
epicsEnvSet("R",            "$(DEV):")
epicsEnvSet("TEMPSCAN",     "1")
epicsEnvSet("CONFSCAN",     "10")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(julabof25hl_DIR)db")

# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

#Load your database defining the EPICS records
iocshLoad("$(julabof25hl_DIR)julabof25hl.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORT=$(PORTNAME), TEMPSCAN=$(TEMPSCAN), CONFSCAN=$(CONFSCAN)")

iocInit()
